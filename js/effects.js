function makeFocusOnThisCard(id_card) {
    var card = document.getElementById('card-'+id_card);
    card.style.zIndex = '1';
    card.style.position = 'fixed';
    card.style.left = '25%';
    card.style.top = '10%';
    card.style.height = '80%';
    card.style.width = '50%';
    card.style.backgroundColor = '#0091ed';

    document.getElementById('btn-make-card-smaller').style.display = 'block';
}

function makeCardSmaller() {
    $('.card').each(function() {
        // if () {

        // }
        if($(this).css('z-index') == 1) {
            var id_card_effects = $(this).attr('id');
            var card_effects = document.getElementById(id_card_effects);

            card_effects.style.zIndex = 'auto';
            card_effects.style.position= 'static';
            card_effects.style.left = '0';
            card_effects.style.top = '0';
            card_effects.style.height = 'auto';
            card_effects.style.width = '100%';
            card_effects.style.backgroundColor = '';

            document.getElementById('btn-make-card-smaller').style.display = 'none';
        }
    });
}

function showListsToLocateThisCard(id_card) {
    var is_there_ul_of_lists = $('#span-show-location-lists-'+id_card).children().length > 0;

    if (is_there_ul_of_lists) {
        $('#ul-lists-to-locate-card').remove();
        // console.log(is_there_ul_of_lists);
    } else {
        // console.log(is_there_ul_of_lists);
        var id_lists_to_locate_card = []
        var title_lists_to_locate_card = []
        $('.list').each(function() {
            
            var list_id = $(this).attr('id').substring(5);
            id_lists_to_locate_card.push(list_id);
        });

        $('.title-list').each(function() {
            var list_title = $(this).text();
            title_lists_to_locate_card.push(list_title);
        });

        content_lists_to_locate_card = '<ul id="ul-lists-to-locate-card">';

        for (var index_lists = 0; index_lists < id_lists_to_locate_card.length; index_lists ++) {
            content_lists_to_locate_card += '<li onclick="locateCardOnThisList(' + id_lists_to_locate_card[index_lists] + ',' + id_card + ')">'+
                title_lists_to_locate_card[index_lists] +
                '</li>';
        }

        content_lists_to_locate_card += '</ul>';

        $('#span-show-location-lists-'+id_card).append(content_lists_to_locate_card);
    }
}

function locateCardOnThisList(id_list, id_card) {
    // var number_id_list = id_list.substring(4);

    // console.log(number_id_list);
    var api_send_id_list_to_locate_card_url = 'http://localhost:8000/updateCardAndListToReplaceCard/'+id_list+'/'+id_card;

    $.post(api_send_id_list_to_locate_card_url,{
            data: id_card
        }, function(data, status) {
            // console.log(data);
        }
    );

    
    moveCardToChosenList(id_list, id_card);
}

function moveCardToChosenList(id_list, id_card) {
    var div_card = document.getElementById('card-'+id_card);
    var list_to_remove_card = document.getElementById('card-'+id_card).parentElement;
    $('#list-'+id_list).append(div_card);
    $('#'+list_to_remove_card).remove(div_card);
}

$(document).ready(function() {
    

    var ul_to_remove = $('#ul-lists-to-locate-card')[0];
    
    if (ul_to_remove) {
        $('#ul-lists-to-locate-card')[0].remove();
    }
});