function createNewList() {
    var api_get_last_id_of_lists_url = 'http://localhost:8000/getLastListId/';
    $.getJSON(api_get_last_id_of_lists_url).done(function (data) {
        var id_of_new_list = 0;
        if (data['data'] != null) {
            id_of_new_list = data['data'][0] + 1;
        } else {
            id_of_new_list = 1;
        }
        generateNewList(id_of_new_list);
        var title_of_new_list = $('#input-add-new-list').val();
        saveDataOfNewList(title_of_new_list);
    });
}

function createNewCard(id_of_span) {
    var api_get_last_id_of_cards_url = 'http://localhost:8000/getLastCardId/';
    $.getJSON(api_get_last_id_of_cards_url).done(function (data) {
        var id_of_new_card = 0;
        if (data['data'] != null) {
            id_of_new_card = data['data'][0] + 1;
        } else {
            id_of_new_card = 1;
        }
        generateNewCard(id_of_span, id_of_new_card);
    });
}

function generateNewList(id_new_list) {
    var title_new_list = $('#input-add-new-list').val();
    var content_new_list =  '<div id="list-' + id_new_list + '" class="col-xs-3 list">\n' +
                            '   <div class="row">\n'+
                            '       <div class="col-xs-12 no-padding">\n'+
                            '           <span id="title-list-' + id_new_list + '" class="title-list">' + title_new_list + '</span>\n' +
                            '       </div>\n'+
                            '   </div>\n'+
                            '   <div class="row">\n'+
                            '       <div class="col-xs-12 no-padding">\n'+
                            '           <div class="col-xs-8 no-padding">\n'+
                            '               <input id="input-add-new-card-' + id_new_list + '" type="text" placeholder="Nueva Tarjeta">\n'+
                            '           </div>\n'+
                            '           <div class="col-xs-2 text-center no-padding border">\n'+
                            '               <button id="span-add-new-card-' + id_new_list + '" class="btn" onclick="createNewCard(this.id)">+</button>\n' +
                            '           </div>\n'+
                            '           <div class="col-xs-2 text-center no-padding padding-b-1_5 border">\n'+
                            '               <button class="btn" onclick="deleteList(' + id_new_list + ')">-</button>\n' +
                            '           </div>\n'+
                            '       </div>\n'+
                            '   </div>\n'+
                            '</div>';
    $('#div-lists').append(content_new_list);
}

function generateNewCard(id_of_span, id_of_new_card) {            
    number_of_id_of_input_title_new_card = id_of_span.substring(18);

    var id_list_of_card_in = document.getElementById('input-add-new-card-'+number_of_id_of_input_title_new_card).parentElement.parentElement.parentElement.parentElement.id;
    
    var number_id_list_card_in = id_list_of_card_in.substring(5);
    
    title_new_card = document.getElementById('input-add-new-card-' + number_of_id_of_input_title_new_card).value;
    // var title_new_card = $('#input-add-new-list').val();
    var content_new_card = '<div class="row" id="row-card-' + id_of_new_card + '">\n'+
                            '   <div id="card-' + id_of_new_card + '" class="col-xs-12 card">\n' +
                            '       <div class="row">\n'+
                            '           <div class="col-xs-6">\n'+
                            '               <span>' + title_new_card + '</span>\n'+
                            '           </div>\n'+
                            '           <div class="col-xs-2 text-center border">\n'+
                            '               <button onclick="makeFocusOnThisCard(' + id_of_new_card + ')">BIG</button>\n'+
                            '           </div>\n'+
                            '           <div class="col-xs-2 text-center no-padding border">\n'+
                            '               <button class="btn" onclick="deleteCard(' + id_of_new_card + ')">-</button>\n'+
                            '           </div>\n'+
                            '           <div class="col-xs-2 text-center no-padding border">\n'+
                            '               <button id="span-show-location-lists-' + id_of_new_card + '" class="btn" onclick="showListsToLocateThisCard(' + id_of_new_card + ')">></button>\n'+
                            '           </div>\n'+
                            '       </div>\n'+
                            '       <div class="row">\n'+
                            '           <div class="col-xs-12">\n'+
                            '               <textarea id="text-' + id_of_new_card + '" name="text-card-' + id_of_new_card + '"></textarea>\n' +
                            '           </div>\n'+
                            '       </div>\n'+
                            '       <div class="row">\n'+
                            '           <div class="col-xs-12 ">\n'+
                            '               <span id="span-save-text-card-' + id_of_new_card + '" onclick="saveDataOfNewTextarea(' + id_of_new_card + ')">guardar</<span>' +
                            '           </div>\n'+
                            '       </div>\n'+
                            '   </div>\n'+
                            '</div>';
    $('#' + id_of_span).parent().parent().parent().parent().append(content_new_card);
    
    saveDataOfNewCard(title_new_card, number_id_list_card_in);
}

function saveDataOfNewList(title_of_new_list) {
    var api_send_new_title_of_list_url = 'http://localhost:8000/saveDataNewList/';

    $.post(api_send_new_title_of_list_url,{
            data: title_of_new_list
        }, function(data, status) {
            // console.log(data);
        }
    );
}

function saveDataOfNewCard(title_new_card, number_id_list_card_in) {
    var api_send_new_title_of_card_url = 'http://localhost:8000/saveDataNewCard/'+number_id_list_card_in;

    $.post(api_send_new_title_of_card_url,{
            data: title_new_card
        }, function(data, status) {
            // console.log(data);
        }
    );
}

$('#input-add-new-list').keypress(function(event){
    var code = event.which || event.keyCode;
    if(code == 13){
        createNewList();
    }
});