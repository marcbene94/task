function deleteCard(id_of_new_card) {
    console.log('card-'+id_of_new_card);

    desactiveCardInDatabase(id_of_new_card);

    deleteDivOfCard(id_of_new_card);
}

function deleteList(id_of_list) {
    // console.log(id_of_list);
    desactiveListInDatabase(id_of_list);

    deleteDivOfList(id_of_list);
}

function desactiveCardInDatabase(id_of_new_card) {
    var api_send_card_to_desactive_url = 'http://localhost:8000/desactiveCard/';

    $.post(api_send_card_to_desactive_url,{
            data: id_of_new_card
        }, function(data, status) {
            // console.log(data);
        }
    );
}

function desactiveListInDatabase(id_of_list) {
    var api_send_list_to_desactive_url = 'http://localhost:8000/desactiveList/';

    $.post(api_send_list_to_desactive_url,{
            data: id_of_list
        }, function(data, status) {
            // console.log(data);
        }
    );
}

function deleteDivOfCard(id_of_new_card) {
    var card = document.getElementById('card-'+id_of_new_card);
    card.remove();
}

function deleteDivOfList(id_of_list) {
    var list = document.getElementById('list-'+id_of_list);
    list.remove();
}