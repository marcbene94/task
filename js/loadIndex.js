$(document).ready(function(){
    loadData();
});

function loadData() {
    var api_get_data_url = 'http://localhost:8000/getAllData/';

    $.getJSON(api_get_data_url).done(function (data) {
        console.log(data);

        printStartDataOfLists(data);
        printStartDataOfCards(data);
    });
}

function printStartDataOfLists(data) {
    if (data && data["data_lists"]) {
        var lists = data["data_lists"];

        var list_content = '';

        for (var index_lists = 0; index_lists < lists.length; index_lists ++) {
            list_content += '<div id="list-' + lists[index_lists][0] + '" class="col-xs-3 list">\n' +
                            '   <div class="row">\n'+
                            '       <div class="col-xs-12 no-padding">\n'+
                            '           <span id="title-list-' + lists[index_lists][0] + '" class="title-list">' + lists[index_lists][1] + '</span>\n' +
                            '       </div>\n'+
                            '   </div>\n'+
                            '   <div class="row">\n'+
                            '       <div class="col-xs-12 no-padding">\n'+
                            '           <div class="col-xs-8 no-padding">\n'+
                            '               <input id="input-add-new-card-' + lists[index_lists][0] + '" type="text" placeholder="Nueva Tarjeta">\n'+
                            '           </div>\n'+
                            '           <div class="col-xs-2 text-center no-padding border">\n'+
                            '               <button id="span-add-new-card-' + lists[index_lists][0] + '" class="btn" onclick="createNewCard(this.id)">+</button>\n' +
                            '           </div>\n'+
                            '           <div class="col-xs-2 text-center no-padding padding-b-1_5 border">\n'+
                            '               <button class="btn" onclick="deleteList(' + lists[index_lists][0] + ')">-</button>\n' +
                            '           </div>\n'+
                            '       </div>\n'+
                            '   </div>\n'+
                            '</div>';
        }

        $('#div-lists').append(list_content);
    }
}

function printStartDataOfCards(data) {
    if (data && data["data_cards"]) {
        var cards = data["data_cards"];

        var card_content = '';

        for (var index_cards = 0; index_cards < cards.length; index_cards ++) {
            card_content =  '<div class="row" id="row-card-' + cards[index_cards][0] + '">\n'+
                            '   <div id="card-' + cards[index_cards][0] + '" class="col-xs-12 card">\n' +
                            '       <div class="row">\n'+
                            '           <div class="col-xs-6">\n'+
                            '               <span>' + cards[index_cards][1] + '</span>\n'+
                            '           </div>\n'+
                            '           <div class="col-xs-2 text-center border">\n'+
                            '               <span onclick="makeFocusOnThisCard(' + cards[index_cards][0] + ')">BIG</span>\n'+
                            '           </div>\n'+
                            '           <div class="col-xs-2 text-center no-padding border">\n'+
                            '               <button class="btn" onclick="deleteCard(' + cards[index_cards][0] + ')">-</button>\n'+
                            '           </div>\n'+
                            '           <div class="col-xs-2 text-center no-padding border">\n'+
                            '               <button id="span-show-location-lists-' + cards[index_cards][0] + '" class="btn" onclick="showListsToLocateThisCard(' + cards[index_cards][0] + ')">></button>\n'+
                            '           </div>\n'+
                            '       </div>\n'+
                            '       <div class="row">\n'+
                            '           <div class="col-xs-12">\n'+
                            '               <textarea id="text-' + cards[index_cards][0] + '" class="text-card" name="text-card-' + cards[index_cards][0] + '">' + cards[index_cards][2] + '</textarea>\n' +
                            '           </div>\n'+
                            '       </div>\n'+
                            '       <div class="row">\n'+
                            '           <div class="col-xs-12 ">\n'+
                            '               <span id="span-save-text-card-' + cards[index_cards][0] + '" onclick="saveDataOfNewTextarea(' + cards[index_cards][0] + ')">guardar</<span>' +
                            '           </div>\n'+
                            '       </div>\n'+
                            '   </div>\n'+
                            '</div>';

            $('#list-'+cards[index_cards][3]).append(card_content);
        }

        $('.text-card').each(function() {
            var text = $(this)[0].innerHTML;

            if (text.includes('---')) {
                console.log(text.indexOf('http'));
                console.log(text.indexOf('html'));
            }
        });
    }
}

function uploadImage(id_card) {
    var url_api = 'http://localhost:8000/uploadImageCard/'+id_card;

    var image = document.getElementById('input-upload-file-'+id_card).files[0];

    var image_name = image['name']

    $.ajax({
        url: url_api, 
        type: 'POST',
        data: image,
        processData: false,
        contentType: false,
        success: function(data) {
            console.log(data);
        }
    });    

    // $.post(url_api, {
    //     data: image
    // },function(data, status) {
    //         console.log(data);
    //     }
    // );
}